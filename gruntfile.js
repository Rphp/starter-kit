module.exports = function(grunt) {
    var path = require('path');

    var img_src  = 'frontend/src/images/';
    var img_dest = 'frontend/public/images/';

    var sass_src = 'frontend/src/sass/';
    var sass_dest = 'frontend/public/css/';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            app: {
                files: {
                      [sass_dest + "style.css"] : [sass_src + "style.scss"]
                }
            }
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd   : sass_dest,
                    src   : ['*.css', '!*.min.css'],
                    dest  : sass_dest,
                    ext   : '.min.css'
                }]
            }
        },

        clean: {
            build: {
                src: [img_dest]
            }
        },

        sprite:{
            all: {
              src: img_src + 'icons/*.png',
              dest: img_dest + 'icons/sprites.png',
              destCss: sass_src + 'sprites/sprites.scss',
              padding: 10
            }
        },

        concat: {
            options: {
                stripBanners: true
            },
            scripts: {
                src: [
                    //--------On/Off components-------
                    // 'bower_components/jquery/dist/jquery.js',
                    // 'bower_components/bootstrap/js/*.js',

                  //   'bower_components/get-size/get-size.js',
                  //   'bower_components/ev-emitter/ev-emitter.js',
                  //   'bower_components/desandro-matches-selector/matches-selector.js',
                  //   'bower_components/fizzy-ui-utils/utils.js',
                  //   'bower_components/outlayer/item.js',
                  //   'bower_components/outlayer/outlayer.js',
                  //   'bower_components/masonry/dist/masonry.pkgd.js',
                  //   'bower_components/masonry/masonry.js',

                    // 'bower_components/bootstrap/js/dropdown.js',
                    // 'bower_components/bootstrap/js/tab.js',
                    // 'bower_components/bootstrap/js/modal.js',
                    // 'bower_components/bootstrap/js/button.js',
                    // 'bower_components/bootstrap/js/collapse.js',
                    // 'bower_components/bootstrap/js/transition.js',
                    // 'bower_components/bootstrap/js/affix.js',

                  //   'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
                  //   'bower_components/slick-carousel/slick/slick.js',
                    'frontend/src/js/*.js'
                ],
                dest: 'frontend/public/js/bundle.js'
            }
        },

        uglify: {
            js: {
                files: {
                    "frontend/public/js/bundle.min.js" : ["frontend/public/js/bundle.js"]
                }
            }
        },

        copy : {
            frontend_fonts  : {
                expand : true,
                cwd    : "frontend/src/fonts",
                src    : "**",
                dest   : "frontend/public/fonts"
            },
            frontend_images: {
                expand : true,
                cwd    : img_src,
                src    : ['**/*'],
                dest   : img_dest
                //,  '!**/icons/**'
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd   : img_dest,
                    src   : ['**/*.{png,jpg,gif}'],
                    dest  : img_dest
                }]
            }
        },

        watch: {
            // options: { livereolad: true },

            css: {
                files    : ["frontend/src/sass/**/*.scss"],
                tasks    : ["sass", "cssmin"],
                options  : {
                    spawn: false
                }
            },
            scripts: {
                    files    : ['frontend/src/js/**/*.js'],
                    tasks    : ["concat", "uglify"],
                options      : {
                    interrupt: true
                }
            }
        }

    });

    grunt.loadNpmTasks("grunt-sass");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks("grunt-contrib-imagemin");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-svg-sprite');
    grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.registerTask('default', ['clean', 'sass', 'concat', 'uglify', 'cssmin', 'copy', 'imagemin']);

};
